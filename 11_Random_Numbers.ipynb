{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 11. Random Numbers\n",
    "---\n",
    "**Content:**\n",
    "- [Random Number Generation](#Random-Number-Generation)\n",
    "- [Different Statistics](#Different-Statistics)\n",
    "- [Integration via Random Numbers / Coordinates](#Integration-via-Random-Numbers-/-Coordinates)\n",
    "- [Multi-Dimensional Integration via Random Numbers / Coordinates](#Multi-Dimensional-Integration-via-Random-Numbers-/-Coordinates)\n",
    "- [Mean Values via Random Numbers](#Mean-Values-via-Random-Numbers)\n",
    "- [Simulating an Experiment via Random Numbers](#Simulating-an-Experiment-via-Random-Numbers)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pylab as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Random Number Generation\n",
    "---\n",
    "Get a random number via Numpy's `random` module, using its `rand()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.random.rand()\n",
    "\n",
    "print( a )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... or a random matrix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.random.rand(3, 4, 2)\n",
    "\n",
    "print( A )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### True vs. Pseudo Random Numbers\n",
    "---\n",
    "> **Note:** Computer algorithms commonly create pseudo random numbers with \"good\" statistics, but being dependent on a seed: \\\n",
    "> `# ... 93802948542771471902796694497278419429380485689050661139408044323 ... ` \\\n",
    "> `#         | start here ...                     | or here ...     `\n",
    "\n",
    "In Numpy we can set the seed on our own using `seed()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(9)\n",
    "\n",
    "a = np.random.rand()\n",
    "print( a )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which will set the whole series of following random numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(6)\n",
    "\n",
    "a = np.random.rand()\n",
    "print( a )\n",
    "\n",
    "a = np.random.rand()\n",
    "print( a )\n",
    "\n",
    "a = np.random.rand()\n",
    "print( a )\n",
    "\n",
    "# --> can be handy to test algorithms !!!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Different Statistics\n",
    "---\n",
    "### `np.random.rand()`: Uniform distribution\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get a random vector \n",
    "N = 1000000\n",
    "b = np.random.rand(N)\n",
    "\n",
    "# and generate a histogram\n",
    "plt.figure(1)\n",
    "plt.hist(b, bins=100, density=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `np.random.normal()`: Normal (Gaussian) distribution\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get a random vector \n",
    "N = 10000\n",
    "b = np.random.normal(size=N, loc=1.5, scale=2.0)\n",
    "\n",
    "# and generate a histogram\n",
    "plt.figure(1)\n",
    "plt.hist(b, bins=30, density=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "?np.random.normal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gauss(mu, sigma, x):\n",
    "    return 1/(sigma * np.sqrt(2 * np.pi)) * np.exp( - (x - mu)**2 / (2 * sigma**2) )\n",
    "\n",
    "plt.figure(1)\n",
    "plt.hist(b, bins=30, density=True)\n",
    "\n",
    "xPlot = np.linspace(-7, 7, 100)\n",
    "plt.plot(xPlot, gauss(mu=1.5, sigma=2.0, x=xPlot), 'r-')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Integration via Random Numbers / Coordinates\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$ A = \\int_0^1 cos^2(x) \\, dx$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return np.cos(x)**2.0\n",
    "\n",
    "xFine = np.linspace(0, 1, 100)\n",
    "\n",
    "plt.figure(1)\n",
    "plt.plot(xFine, f(xFine))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get random `x` and `y` coordinates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = np.random.rand(2, 50)\n",
    "\n",
    "plt.figure(1)\n",
    "plt.plot(x, y, 'o')\n",
    "plt.plot(xFine, f(xFine))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now mark points below and above `f(x)`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = np.random.rand(2, 50)\n",
    "\n",
    "indAbove = np.where( y > f(x)  )[0]\n",
    "indBelow = np.where( y <= f(x) )[0]\n",
    "\n",
    "plt.figure(1)\n",
    "plt.plot(x[indAbove], y[indAbove], 'o')\n",
    "plt.plot(x[indBelow], y[indBelow], 'o')\n",
    "plt.plot(xFine, f(xFine))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And finally get the integral of `f(x)` from `0` to `1` by comparing the amount of points below and above `f(x)`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = np.random.rand(2, 5000)\n",
    "\n",
    "indAbove = np.where( y > f(x)  )[0]\n",
    "indBelow = np.where( y <= f(x) )[0]\n",
    "\n",
    "intRand = len(indBelow) / ( len(indBelow) + len(indAbove) )\n",
    "\n",
    "print( intRand )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare to Scipy's `quad()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.integrate as integrate\n",
    "intQuad, error = integrate.quad(f, a=0, b=1)\n",
    "\n",
    "print( intRand, \"vs.\", intQuad )\n",
    "print( \"error = \", intRand-intQuad )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multi-Dimensional Integration via Random Numbers / Coordinates\n",
    "---\n",
    "$$ A = \\int_{0}^{2} \\int_{0}^{2} \\frac{ e^{-x\\,t} }{t^2 + 1}  \\, dx \\, dt$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x,t):\n",
    "    return np.exp(-x*t) / (t**2 + 1)\n",
    "\n",
    "# get random numbers\n",
    "N = 50\n",
    "x, t = np.random.rand(2, N) * 2.0\n",
    "y    = np.random.rand(N, N) * 2.0\n",
    "\n",
    "# get the volume of the FULL space we have been sampling\n",
    "volume = 2.0 * 2.0 * 2.0\n",
    "\n",
    "# create a meshgrid of our random (x,t) coordinates\n",
    "# so that we can call f(xGrid, tGrid)\n",
    "xGrid, tGrid = np.meshgrid(x, t)\n",
    "\n",
    "# count how often our random y(x,t) lays below f(x,t)\n",
    "indBelow = np.where( y <= f(xGrid, tGrid) )\n",
    "\n",
    "# and calculate the interal with the proper normalization!\n",
    "intRand = len(indBelow[0]) * volume / N**2.0\n",
    "\n",
    "# multi-dimensional integral via scipy's integrate.nquad\n",
    "intQuad, error = integrate.nquad(f, [[0, 2],\n",
    "                                     [0, 2]])\n",
    "\n",
    "print( intRand, \"vs.\", intQuad )\n",
    "print( \"error = \", intRand-intQuad )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Mean Values via Random Numbers\n",
    "---\n",
    "$$\n",
    "f(x) = sin \\left( \\frac{ 1 }{x(2-x)} \\right)^2   \n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return (np.sin(1/(x*(2-x))))**2\n",
    "\n",
    "N = 200\n",
    "x = np.linspace(0.0001, 1.9999, N)\n",
    "y = f(x)\n",
    "\n",
    "plt.figure(1)\n",
    "plt.plot(x, y)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's calculate the standard mean for a variety of starting points `x0`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 10 # 100, 1000, 10000 (we need 10000 to be sure)\n",
    "for x0 in np.logspace(0, -6, 7):\n",
    "    \n",
    "    print( \"x0 = %f : mean = %f\"%(x0, f(np.linspace(x0, 1, N)).mean() ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's calculate the standard mean for a variety of `N` but using random numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for N in range(100, 1000, 100):\n",
    "    \n",
    "    print( \"N = %d : mean = %f\"%(N, f(2.0 * np.random.rand(N)).mean()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulating an Experiment via Random Numbers\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 100\n",
    "# create x on a random grid\n",
    "x = np.random.rand(N)\n",
    "\n",
    "# and y = 2.0 * x - 1.0 \n",
    "# ... and add Gaussian noise (like in an experiment) to it\n",
    "y = 2.0 * x - 1.0 + np.random.normal(scale=0.5, size=N)\n",
    "\n",
    "# and plot everything\n",
    "plt.figure(1)\n",
    "plt.plot(x, y, 'o')\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load curve_fit from scipy\n",
    "from scipy.optimize import curve_fit\n",
    "\n",
    "# define a linear function\n",
    "def fitFunc(x, a, b):\n",
    "    return a * x + b\n",
    "\n",
    "# and fit it to the noisy data\n",
    "myFit, V = curve_fit(fitFunc, x, y)\n",
    "\n",
    "# get error estimate\n",
    "error = np.sqrt(np.diag(V))  \n",
    "\n",
    "# print out fitting parameters\n",
    "print(\" a = %+4.3f +/- %4.3f\"%(myFit[0], error[0]))\n",
    "print(\" b = %+4.3f +/- %4.3f\"%(myFit[1], error[1]))\n",
    "\n",
    "# and plot everything\n",
    "plt.figure(1)\n",
    "plt.plot(x, y, 'o')\n",
    "plt.plot(x, fitFunc(x, *myFit))\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# let's have a look into the STATISTICS of our fitting parameters\n",
    "\n",
    "N     = 1000          # Number of random sets ... for the statistics\n",
    "a     = np.zeros(N)   # array to save all fitted a \n",
    "b     = np.zeros(N)   # array to save all fitted a \n",
    "\n",
    "# loop over sets\n",
    "for i in range(N):\n",
    "    \n",
    "    # create a new set\n",
    "    x = np.random.rand(50)\n",
    "    y = 2.0 * x - 1.0 + np.random.normal(scale=0.15, size=50)\n",
    "    \n",
    "    # and fit it to the noisy data\n",
    "    myFit, V = curve_fit(fitFunc, x, y)\n",
    "    \n",
    "    # get error estimate\n",
    "    error = np.sqrt(np.diag(V))  \n",
    "\n",
    "    # save a and b\n",
    "    a[i] = myFit[0]\n",
    "    b[i] = myFit[1]\n",
    "    \n",
    "#plt.subplot(2, 1, 1)\n",
    "plt.hist(a, bins=int(np.sqrt(N)), label='$a$' )\n",
    "plt.hist(b, bins=int(np.sqrt(N)), label='$b$' )\n",
    "plt.legend() \n",
    "\n",
    "#plt.subplot(2,1,2)\n",
    "#plt.hist(b, bins=int(np.sqrt(N)), label='$b$' )\n",
    "#plt.legend() \n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Convex hull of a square\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# function that returns all points of A (set of x/y points)\n",
    "# which belong to its convex hull\n",
    "def getHull(A):\n",
    "    \n",
    "    N = np.size(A[:,0])\n",
    "    \n",
    "    # create an empty list of corner indices\n",
    "    ind = []\n",
    "\n",
    "    # add the top-most (largest y position) point to the list of corner indices\n",
    "    # ... since it belongs to our convex hull by definition\n",
    "    ind.append( np.argmax(A[:,1]) )\n",
    "\n",
    "    # initial values for our search\n",
    "    lastAngle  = -4.0\n",
    "    closed     = False\n",
    "\n",
    "    # search for all points of the convex hull via while loop\n",
    "    # ... continue the search as long as the hull is not closed\n",
    "    while (not closed):\n",
    "        \n",
    "        # Algorithm idea: find the next point with smallest angle \n",
    "        #                 larger than previous angle\n",
    "\n",
    "        # generate a list of angles between our last point and all other points\n",
    "        newAngles = np.arctan2(A[:, 1] - A[ind[-1], 1], \n",
    "                               A[:, 0] - A[ind[-1], 0])\n",
    "        \n",
    "        # \"penalty\" for angle = 0\n",
    "        newAngles[np.where(newAngles == 0)] += 1000.0\n",
    "        \n",
    "        # find the smallest angle from our \"penalized\" angle list\n",
    "        nextInd = np.argmin(np.abs(lastAngle - newAngles))\n",
    "\n",
    "        # check if the next index is already in our list\n",
    "        if(nextInd in ind):\n",
    "            # ... if yes the hull is closed\n",
    "            closed = True\n",
    "            \n",
    "        else:\n",
    "            # ... if not we save the angle between the last and the next\n",
    "            #            angle as our \"lastAngle\"\n",
    "            lastAngle = np.arctan2(A[nextInd,1] - A[ind[-1],1],\n",
    "                                   A[nextInd,0] - A[ind[-1],0])\n",
    "            \n",
    "            # and add the nextIndex to the list of our hull array\n",
    "            ind.append( nextInd )\n",
    "            \n",
    "    return ind\n",
    "\n",
    "\n",
    "# generate N random (x,y)\n",
    "N = 20\n",
    "A = np.random.rand(N, 2)\n",
    "\n",
    "# and plot them\n",
    "plt.plot(A[:,0], A[:,1], 'o')\n",
    "\n",
    "ind = getHull(A)\n",
    "    \n",
    "# plot line connecting all corner indices\n",
    "for ii in range(np.size(ind)):\n",
    "    plt.plot( A[[ind[ii-1], ind[ii]], 0],\n",
    "              A[[ind[ii-1], ind[ii]], 1], 'r')\n",
    "    \n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Convex hull of a circle\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate N random (x,y)\n",
    "N = 1000\n",
    "A = np.random.rand(N, 2) * 2.0 - 1.0\n",
    "\n",
    "# get all points which belong to a circle of with radius = 1\n",
    "ind = np.where( np.sqrt( A[:,0]**2.0 + A[:,1]**2.0) <= 1 )[0]\n",
    "\n",
    "# and plot them\n",
    "plt.plot(A[:,0], A[:,1], 'or')\n",
    "plt.plot(A[ind,0], A[ind,1], 'og')\n",
    "plt.axis('equal')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate N random (x,y)\n",
    "N = 500\n",
    "A = np.random.rand(N, 2) * 2.0 - 1.0\n",
    "\n",
    "ind = np.where( np.sqrt( A[:,0]**2.0 + A[:,1]**2.0) <= 1 )[0]\n",
    "\n",
    "# and plot them\n",
    "plt.plot(A[ind,0], A[ind,1], 'o')\n",
    "\n",
    "circle = A[ind,:]\n",
    "\n",
    "ind = getHull(circle)\n",
    "    \n",
    "# plot line connecting all corner indices\n",
    "for ii in range(np.size(ind)):\n",
    "    plt.plot( circle[[ind[ii-1], ind[ii]], 0],\n",
    "              circle[[ind[ii-1], ind[ii]], 1], 'r')\n",
    "    \n",
    "plt.axis('equal') \n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
