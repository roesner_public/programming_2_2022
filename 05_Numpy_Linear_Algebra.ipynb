{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5. Numpy Linear Algebra\n",
    "---\n",
    "\n",
    "Numpy provides a powerful N-dimensional array object and tools for working with these arrays, which makes it specifically attractive for linear algebra tasks.\n",
    "\n",
    "**Content:**\n",
    "- [Vectors, Matrices & Dot Products](#Vectors,-Matrices-&-Dot-Products)\n",
    "- [Transpose, Trace, Determinant, Norm, Inverse](#Transpose,-Trace,-Determinant,-Norm,-Inverse)\n",
    "- [Solving Linear Equations](#Solving-Linear-Equations)\n",
    "- [Eigenvalue Problems](#Eigenvalue-Problems)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vectors, Matrices & Dot Products\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import Numpy and define vectors (1D arrays):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "a = np.array([1,0,0])\n",
    "b = np.array([0,1,0])\n",
    "c = np.array([1,1,1])\n",
    "\n",
    "print( \"a:\", a )\n",
    "print( \"b:\", b )\n",
    "print( \"c:\", c )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get vector products via `np.dot(a,b)`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print( np.dot(a, b) )\n",
    "print( np.dot(a, c) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** This is different to the element wise product!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print( a * b )\n",
    "print( a * c )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vector-matrix dot products do not commute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "matA = np.array([[8, 7, 6],\n",
    "                 [5, 4, 3],\n",
    "                 [2, 1, 0]])\n",
    "\n",
    "matB = np.arange(0,9).reshape(3,3)\n",
    "\n",
    "print( np.dot(matA, c), \"=!\", np.dot(c, matA) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matrix-matrix dot products do not commute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print( np.dot(matA, matB) )\n",
    "print( \"=!\" )\n",
    "print( np.dot(matB, matA) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`A @ B` is the shorthand for `np.dot(A,B)`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "matA @ matB"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transpose, Trace, Determinant, Norm, Inverse\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# let's start with a matrix\n",
    "matA = np.arange(0,9).reshape(3,3)\n",
    "print( matA )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# do the standard transpose\n",
    "print( np.transpose(matA) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ... or via MAT.transpose()\n",
    "print( matA.transpose() )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ... or via MAT.T\n",
    "print( matA.T )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# and now get the trace\n",
    "print( matA.trace() )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get the determinant\n",
    "# ... via the linalg package\n",
    "\n",
    "print( np.linalg.det(matA) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get the norm\n",
    "print( np.linalg.norm(matA) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# and now the inverse\n",
    "# ... via the linalg package\n",
    "# ... and for a non-singular matrix\n",
    "matA = np.array([[2,3],\n",
    "                 [1,2]])\n",
    "\n",
    "matB = np.linalg.inv(matA)\n",
    "\n",
    "print( matB )\n",
    "print( np.dot(matA, matB) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solving Linear Equations\n",
    "---\n",
    "\n",
    "Let's assume we want to solve the following set of equations:\n",
    "$$ \n",
    "\\begin{align}\n",
    " x& + 3y + 5z &=& 10 \\\\\n",
    "2x& + 5y +  z &=&  8 \\\\\n",
    "2x& + 3y + 8z &=&  3\n",
    "\\end{align}\n",
    "$$\n",
    "which we can cast into:\n",
    "$$\n",
    "\\begin{align}\n",
    "    \\begin{pmatrix}\n",
    "    x \\\\ y \\\\ z\n",
    "    \\end{pmatrix}\n",
    "    =\n",
    "    \\begin{pmatrix}\n",
    "    1 & 3 & 5 \\\\\n",
    "    2 & 5 & 1 \\\\\n",
    "    2 & 3 & 8\n",
    "    \\end{pmatrix}^{-1}\n",
    "    \\begin{pmatrix}\n",
    "    10 \\\\ 8 \\\\ 3\n",
    "    \\end{pmatrix}\n",
    "\\end{align}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define matrix and solution vector\n",
    "M = np.array([[1, 3, 5],\n",
    "              [2, 5, 1],\n",
    "              [2, 3, 8]])\n",
    "\n",
    "v = np.array([10,\n",
    "               8,\n",
    "               3])\n",
    "\n",
    "# and get the solution via the inverse of M and the dot product\n",
    "x = np.dot( np.linalg.inv(M), v )\n",
    "\n",
    "print( x )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# check it\n",
    "\n",
    "print( np.dot( M, x ) ) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# now, solve it via np.linalg.solve\n",
    "x = np.linalg.solve(M, v)\n",
    "\n",
    "print( x )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# So, what's the difference?\n",
    "# --> Performance / time!\n",
    "\n",
    "# let's setup a large set of equations\n",
    "M_big = np.arange(0,25,0.0001, float).reshape(500,500)\n",
    "v_big = np.arange(50,0,-0.1).reshape(500,1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# time the inverse version\n",
    "%timeit x_big = np.dot( np.linalg.inv(M_big), v_big )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# time the solve version\n",
    "%timeit x_big = np.linalg.solve(M_big, v_big)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Eigenvalue Problems\n",
    "---\n",
    "\n",
    "Let's get all eigenvalues $\\lambda$ and eigenvectors $\\vec{v}$ of a square-matrix $A$:\n",
    "\n",
    "$$\n",
    " \\large\n",
    " A \\vec{v}_i = \\lambda_i \\vec{v}_i\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# define matrix A\n",
    "A = np.array([[1, 4, 0],\n",
    "              [0, 2, 7],\n",
    "              [0, 5, 3]])\n",
    "\n",
    "# use the linalg package to get lambda (l) and eigenvectors (v)\n",
    "l, v = np.linalg.eig(A)\n",
    "\n",
    "print( \"eigenvalues:\" )\n",
    "print( l )\n",
    "print( \"eigenvectors:\" )\n",
    "print( v )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "simple check via: $ A v - \\lambda v = 0$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# let's check this:\n",
    "print( np.dot(A, v) - l * v )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "another check via: $ \\vec{v}_i^t A \\vec{v}_i = \\lambda_i$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# let's check it again for a single eigenvalue\n",
    "print( np.dot( np.dot(v[:,1].transpose(), A), v[:,1]) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# let's check it again ... for all eigenvalues\n",
    "print( np.diag( np.dot( np.dot(v.transpose(), A), v) ) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# now, we can also setup the characteristic polynomial\n",
    "poly = np.poly(l)\n",
    "\n",
    "print( poly )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# and get its roots\n",
    "roots = np.roots( poly )\n",
    "\n",
    "print( roots )"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
