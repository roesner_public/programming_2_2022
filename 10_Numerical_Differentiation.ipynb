{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 10. Numerical Differentiation\n",
    "---\n",
    "**Content:**\n",
    "- [Numerical Differentiation of Analytic Functions](#Numerical-Differentiation-of-Analytic-Functions)\n",
    "- [Numerical Differentiation of Sampled Data](#Numerical-Differentiation-of-Sampled-Data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pylab as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Problem & Its Solution\n",
    "---\n",
    "We aim to calculate:\n",
    "$$ \n",
    "\\begin{align*}\n",
    "    f'(x) &= \\frac{df(x)}{dx} \\\\\n",
    "          &\\approx \\frac{f(x+d) - f(x)}{d} \\\\\n",
    "          &\\approx \\frac{f(x+d) - f(x-d)}{2d} \\\\\n",
    "    \\text{or} \\quad f''(x) &= \\frac{d^2f(x)}{dx^2}  \\approx \\dots\n",
    "\\end{align*}\n",
    "$$\n",
    "\n",
    "$\\Rightarrow$ Again, we could implement all on our own. Or, again, we could use the predefined functions from Numpy and Scipy!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numerical Differentiation of Analytic Functions\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return np.sin(x)\n",
    "\n",
    "def fp(x):\n",
    "    return np.cos(x)\n",
    "\n",
    "def fpp(x):\n",
    "    return -np.sin(x)\n",
    "\n",
    "def fppp(x):\n",
    "    return -np.cos(x)\n",
    "\n",
    "x = np.linspace(0, 2.0*np.pi, 100)\n",
    "\n",
    "plt.figure(1)\n",
    "\n",
    "plt.plot(x, f(x),    label=\"$f(x)    = sin(x)$\")\n",
    "plt.plot(x, fp(x),   label=\"$f'(x)   = cos(x)$\")\n",
    "plt.plot(x, fpp(x),  label=\"$f''(x)  = -sin(x)$\")\n",
    "plt.plot(x, fppp(x), label=\"$f'''(x) = -cos(x)$\")\n",
    "\n",
    "plt.xlabel(\"$x$\")\n",
    "plt.ylabel(\"$f(x)$\")\n",
    "plt.legend(loc=1)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's use Scipy's `misc` module and within this the `misc.derivative()` function to calucalte the first derivative:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.misc as misc\n",
    "\n",
    "# the point at which we like to calculate the derivative\n",
    "x0 = 1.23\n",
    "\n",
    "# to calculate the 1. derivative of our function f(x) = sin(x)\n",
    "df = misc.derivative(f, x0=x0, n=1, dx=0.1)\n",
    "\n",
    "# plot it again\n",
    "plt.figure(1)\n",
    "\n",
    "plt.plot(x, fp(x),     label=\"$f'(x) = cos(x)$\")\n",
    "plt.plot(x0, df, 'ro', label=\"$f'_{num}(x=%3.2f)$\"%x0)\n",
    "\n",
    "plt.xlabel(\"$x$\")\n",
    "plt.ylabel(\"$f'(x)$\")\n",
    "plt.legend(loc=1)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and all other derivatives:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df   = misc.derivative(f, x0=x0, n=1, dx=0.1)\n",
    "ddf  = misc.derivative(f, x0=x0, n=2, dx=0.1)\n",
    "dddf = misc.derivative(f, x0=x0, n=3, dx=0.1, order=5)\n",
    "\n",
    "# plot it again\n",
    "plt.figure(1)\n",
    "\n",
    "plt.plot(x, fp(x),   label=\"$f'(x)   = cos(x)$\")\n",
    "plt.plot(x, fpp(x),  label=\"$f''(x)  = -sin(x)$\")\n",
    "plt.plot(x, fppp(x), label=\"$f'''(x) = -cos(x)$\")\n",
    "\n",
    "plt.plot(x0, df,   'bo', label=\"$f'_{num}(x=%3.2f)$\"%x0)\n",
    "plt.plot(x0, ddf,  'yo', label=\"$f''_{num}(x=%3.2f)$\"%x0)\n",
    "plt.plot(x0, dddf, 'go', label=\"$f'''_{num}(x=%3.2f)$\"%x0)\n",
    "\n",
    "plt.xlabel(\"$x$\")\n",
    "plt.ylabel(\"$f'(x)$\")\n",
    "plt.legend(loc=1)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numerical Differentiation of Sampled Data\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define some sample data\n",
    "xi = np.linspace(0, 6, 50)\n",
    "yi = np.sin(xi/0.5) * np.exp(-xi)\n",
    "\n",
    "plt.figure(1)\n",
    "plt.plot(xi, yi, 'bo')\n",
    "\n",
    "plt.xlabel(\"$x$\")\n",
    "plt.ylabel(\"$f(x)$\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strategy 1: Fit and use the fit-function in Scipy's `derivative()`:\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# using, for example, a polynomial function\n",
    "myFit = np.polyfit(xi, yi, deg=9)\n",
    "\n",
    "# create a call-able function out of our fit coefficients\n",
    "fFit = np.poly1d(myFit)\n",
    "\n",
    "# the point at which we like to calculate the derivative\n",
    "x0 = 2.5\n",
    "\n",
    "# get the derivative from scipy's derivative function\n",
    "df = misc.derivative(fFit, x0=x0, n=1, dx=0.01)\n",
    "\n",
    "# plot it\n",
    "xfine = np.linspace(np.min(xi), np.max(xi), 100)\n",
    "\n",
    "plt.plot(xi, yi, 'bo', label=\"$f(x)$\")\n",
    "plt.plot(xfine, fFit(xfine), 'r-', label=\"$f_{fit}(x)$\")\n",
    "plt.text(2.0, 0.3, \"$f'(%2.1f) = %4.3f$\"%(x0,df), fontsize=16)\n",
    "\n",
    "plt.xlabel(\"$x$\")\n",
    "plt.ylabel(\"$f(x)$\")\n",
    "plt.legend()\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strategy 2: Using Numpy's `diff()`:\n",
    "---\n",
    "> **Note**: diff returns just $f(x_{i+1}) - f(x_i)$ so we need to divide by $d = x_i - x_{i+1}$ on our own."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = np.diff(yi) / (xi[1] - xi[0])\n",
    "\n",
    "# find the index of xi which is closest to xx\n",
    "# (the point at which we'd like to evaluate the differentiation)\n",
    "ind = np.argmin(np.abs(xi - x0))\n",
    "\n",
    "plt.text(2.0, 1.0, \"$f'(%2.1f) = %4.3f$\"%(x0,df[ind]), fontsize=16)\n",
    "\n",
    "plt.plot(xi, yi, 'bo', label=\"$f(x)$\")\n",
    "plt.plot(xi[1:], df, 'g-', label=\"$f'_{num}(x)$\") \n",
    "\n",
    "plt.xlabel(\"$x$\")\n",
    "plt.ylabel(\"$f(x)$\")\n",
    "plt.legend()\n",
    "\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
